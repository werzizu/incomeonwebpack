var React = require('react');
import ListItem from 'material-ui/List';
require('./Item.css')

var Item = React.createClass({
    render: function () {
        return (
            <span>
                {this.props.name} : {this.props.price}
            </span>
        )
    }
});

module.exports = Item;