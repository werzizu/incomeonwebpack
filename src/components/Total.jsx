var React = require("react");

var Total = React.createClass({
    render: function () {
        return (
            <h2>{this.props.total}</h2>
        )
    }
});

module.exports = Total;