var React = require('react');
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

var ItemPrice = React.createClass({

    getInitialState: function () {
        return {
            name: '',
            price: ''
        }
    },

    addNewItem: function (income) {
        var item = {
            name: this.state.name,
            price: Number(this.state.price),
            income: income
        };
        this.props.addItem(item);
        this.setState({name: '', price: ''})
    },

    changeText: function (e) {
        this.setState({name: e.target.value})
    },

    changePrice: function (e) {
        const nonNumericRegex = /[^0-9.]+/g;
        let newValue = e.target.value.replace(nonNumericRegex, "");
        this.setState({price: newValue});
    },

    render: function () {
        return (
           <div>
               <TextField
                   type="text"
                   floatingLabelText="please provide the name"
                   hintText="Car"
                   value={this.state.name}
                   onChange={this.changeText}
               />
               <TextField
                   hintText="7499"
                   floatingLabelText="How much"
                   value={this.state.price}
                   onChange={this.changePrice}
               />
               <RaisedButton onClick={this.addNewItem.bind(null, true)}> + </RaisedButton>
               <RaisedButton onClick={this.addNewItem.bind(null, false)}> - </RaisedButton>
           </div>
        )
    }
});

module.exports = ItemPrice;