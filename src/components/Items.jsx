var React = require('react');
import {List, ListItem} from 'material-ui/List';
import {redA100, greenA100} from 'material-ui/styles/colors'

var Item = require('./Item.jsx');

var Items = React.createClass({
    render : function () {
        return (
            <List className="items">
                {
                    this.props.items.map(function (item, i) {
                        return (
                            <ListItem style={{backgroundColor: item.income ? greenA100 : redA100}} key={i}>
                                <Item
                                    income={item.income}
                                    name={item.name}
                                    price={item.price}
                                />
                            </ListItem>
                        )
                    })
                }
            </List>
        )
    }
});

module.exports = Items;