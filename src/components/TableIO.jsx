var React = require('react');
var ItemPrice = require('./ItemPrice.jsx');
var Items = require('./Items.jsx');
var Total = require('./Total.jsx');
import Paper from 'material-ui/Paper';
require('./TableIO.css');

var TableIO = React.createClass({
    getInitialState: function () {
      return {
          items: [
              {
                  name: 'fly tickets',
                  price: 300,
                  income: false
              },
              {
                  name: 'salary',
                  price: 900,
                  income: true
              },
              {
                  name: 'apt rent',
                  price: 500,
                  income: false
              }
          ]
      }
    },

    addItem: function (item) {
        var items = this.state.items.slice();
        items.unshift(item);
        this.setState({items: items});
    },
    render: function () {
        var total = 0;
        for(var i = 0; i < this.state.items.length; i++){
            this.state.items[i].income ? total += this.state.items[i].price : total -= this.state.items[i].price
        }
        const style = {
            width: 300,
            margin: 20,
            textAlign: 'center',
            display: 'inline-block',
        };
        return (
            <Paper style={style} zDepth={3}>
                <h4 className="TableIO"> Table Income/Outcome</h4>
                <Total total={total} />
                <ItemPrice addItem={this.addItem} />
                <Items items={this.state.items} />
            </Paper>
        )
    }
});

module.exports = TableIO;