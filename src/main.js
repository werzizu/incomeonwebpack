import ReactDOM from 'react-dom';
import React from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

var TableIO = require('./components/TableIO.jsx');


const App = function(){
    return (
        <MuiThemeProvider>
            <TableIO />
        </MuiThemeProvider>
    )
};
ReactDOM.render(
    <App />,
    document.getElementById('mount-point')
)